import 'dart:developer';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key});

  void logout() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Stack(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                ),
                child: Container(
                  width: double.infinity,
                  height: 200,
                  child: Image.asset(
                    'assets/bg_home.jpg',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              // Positioned(
              //   top: 30,
              //   left: 50,
              //   child: Align(
              //     alignment: Alignment.topLeft,
              //     child: SizedBox(
              //       width: 322,
              //       height: 166,
              //       child: ClipRRect(
              //         borderRadius: BorderRadius.circular(20),
              //         child: Image.asset(
              //           'assets/blue_home.jpg',
              //           fit: BoxFit.cover,
              //         ),
              //       ),
              //     ),
              //   ),
              // ),
            ],
          ),
          SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 350,
                height: 50,
                child: TextField(
                  decoration: InputDecoration(
                    labelText: 'Search',
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Expanded(
                child: Container(
                  margin: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/flight_logo.jpg',
                        width: 50, // Ukuran lebar gambar
                        height: 50, // Ukuran tinggi gambar
                        fit: BoxFit.cover, // Mengatur tata letak gambar
                      ),
                      TextButton(
                        onPressed: () {
                          // Aksi yang ingin dilakukan ketika tombol ditekan
                        },
                        child: Text('Flight'),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/turkiye_logo.png',
                        width: 80, // Ukuran lebar gambar
                        height: 50, // Ukuran tinggi gambar
                        fit: BoxFit.cover, // Mengatur tata letak gambar
                      ),
                      TextButton(
                        onPressed: () {
                          // Aksi yang ingin dilakukan ketika tombol ditekan
                        },
                        child: Text('Favorite Tour'),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/hotel_logo.jpg',
                        width: 50, // Ukuran lebar gambar
                        height: 50, // Ukuran tinggi gambar
                        fit: BoxFit.cover, // Mengatur tata letak gambar
                      ),
                      TextButton(
                        onPressed: () {
                          // Aksi yang ingin dilakukan ketika tombol ditekan
                        },
                        child: Text('Hotel Tour'),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/rule_logo.jpg',
                        width: 50, // Ukuran lebar gambar
                        height: 50, // Ukuran tinggi gambar
                        fit: BoxFit.cover, // Mengatur tata letak gambar
                      ),
                      TextButton(
                        onPressed: () {
                          // Aksi yang ingin dilakukan ketika tombol ditekan
                        },
                        child: Text('Rule'),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Expanded(
                child: Container(
                  margin: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/needs_logo.jpg',
                        width: 50, // Ukuran lebar gambar
                        height: 50, // Ukuran tinggi gambar
                        fit: BoxFit.cover, // Mengatur tata letak gambar
                      ),
                      TextButton(
                        onPressed: () {
                          // Aksi yang ingin dilakukan ketika tombol ditekan
                        },
                        child: Text('Needs'),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/guide_logo.jpg',
                        width: 80, // Ukuran lebar gambar
                        height: 50, // Ukuran tinggi gambar
                        fit: BoxFit.cover, // Mengatur tata letak gambar
                      ),
                      TextButton(
                        onPressed: () {
                          // Aksi yang ingin dilakukan ketika tombol ditekan
                        },
                        child: Text('TourGuide'),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/asuransi_logo.jpg',
                        width: 50, // Ukuran lebar gambar
                        height: 50, // Ukuran tinggi gambar
                        fit: BoxFit.cover, // Mengatur tata letak gambar
                      ),
                      TextButton(
                        onPressed: () {
                          // Aksi yang ingin dilakukan ketika tombol ditekan
                        },
                        child: Text('Travel Insurance'),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/tourist_logo.png',
                        width: 50, // Ukuran lebar gambar
                        height: 50, // Ukuran tinggi gambar
                        fit: BoxFit.cover, // Mengatur tata letak gambar
                      ),
                      TextButton(
                        onPressed: () {
                          // Aksi yang ingin dilakukan ketika tombol ditekan
                        },
                        child: Text('Tourist'),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 0),
          Row(
            children: [
              Expanded(
                child: Container(
                  margin: EdgeInsets.all(25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Image.asset(
                        'assets/more_logo.png',
                        width: 50, // Ukuran lebar gambar
                        height: 50, // Ukuran tinggi gambar
                        fit: BoxFit.cover, // Mengatur tata letak gambar
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: TextButton(
                          onPressed: () {
                            // Aksi yang ingin dilakukan ketika tombol ditekan
                          },
                          child: Text('More'),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),

          // Text("Selamat Datang di Home page"),
          ElevatedButton.icon(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(Colors.red),
            ),
            onPressed: logout,
            icon: Icon(Icons.logout_outlined),
            label: Text("Logout"),
          ),
        ],
      ),
    );
  }
}
